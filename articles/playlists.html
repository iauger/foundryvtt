<p>Foundry Virtual Tabletop supports the use of audio to accentuate player actions or enrich the atmosphere during gaming sessions. This page details the use of Playlists and the sounds contained within them.</p>
<hr>

<h2 id="directory">The Playlist Directory</h2>

<p>The Playlist Directory allows Gamemasters and Assistants to create new Playlists and modify existing ones. All players can access this sidebar, to view the currently playing playlists and adjust client-side volume controls.</p>

<h3 id="controls">Global Volume Controls</h3>
<p>Global volume controls are client-sided sliders that modify all sounds of certain categories, allowing players to fine-tune volume levels on their end.</p>

<p>The <b>Playlists</b> volume slider determines the master volume of playlists played by the Gamemaster.</p>
<p>The <b>Ambient</b> volume slider adjusts the master volume of Ambient Sounds heard in a Scene.</p>
<p>The <b>Interface</b> volume slider adjusts the master volume of sounds triggered through interface actions (such as chat messages and dice rolls). </p>

<h3 id="create">Creating Playlists</h3>
<p>In order to play audio tracks on demand, or as a sequence, they must be first added to a playlist. A playlist can contain multiple sounds, and the same sound can be in multiple playlists.</p>
<p>Click the "Create Playlist" button at the bottom of the Playlist Directory sidebar to create a playlist. From there, a prompt will appear, allowing you to enter the name of the playlist.</p>
<p class="note info">Playlists are added to the bottom of the list, but are sorted in alphabetical order once Foundry is refreshed.</p>

<h3 id="manage">Managing Playlists</h3>

<figure class="right">
    @Image[43]
    <figcaption>The Gamemaster's view of the Playlist Directory sidebar.</figcaption>
</figure>

<p>Playlists on their own come with various buttons and toggles used to manage both the playlist itself and the playback of the sounds it contains.</p>

<dl>
    <dt>Edit Playlist</dt>
    <dd>Opens a prompt to edit the playlist's name and its Playback Mode</dd>
    <dt>Add Sound</dt>
    <dd>Adds a Sound to the playlist. See the "Adding Sounds" section in this article.</dd>
    <dt>Delete Playlist</dt>
    <dd>Deletes the playlist after accepting a confirmation prompt.</dd>
    <dt>Playback Mode</dt>
    <dd>Cycles through the various playback modes. Playback Mode determines the behavior of the playlist when the "Play Playlist" button is pressed.<br>
    <b>Sequential Playback:</b> Plays each sound in the playlist, one at a time, in order.<br>
    <b>Shuffle Tracks:</b> Plays each sound in the playlist, one at a time, in a random order.<br>
    <b>Simultaneous Playback:</b> Plays each sound in the playlist at the same time.<br>
    <b>Soundboard Only: </b> Does not allow the playlist to be played as a whole. The playlist will always be expanded when Foundry reloads, and each sound can be played separately as normal.
    </dd>
    <dt>Play/Stop Playlist</dt>
    <dd>Plays the tracks in the playlist according to the Playlist Mode. If stopping, all currently playing tracks in the playlist will stop playing.</dd>
</dl>

<p class="note info">Normally, only Gamemasters and Assistant users can view and manage playlists, but all players can see the names of playlists when they are played. Take care to keep your playlist and sound names spoiler-free!</p>

<h3 id="add">Adding Sounds</h3>
<figure class="right">
    @Image[44]
    <figcaption>The New Track configuration window.</figcaption>
</figure>

<p>Once a playlist has been created, individual sounds can be added to it by clicking the "Add Sound" button next to the playlist (notated by a plus sign icon). This will bring up the configuration window allowing you to add the sound to the chosen playlist. Once a sound has been created in a playlist, the playlist can be expanded to manage the individual sound, such as editing the name or sound source, toggling the track to loop when played, or deleting the sound from the playlist.</p>

<dl>
    <dt>Track Name</dt>
    <dd>The name of the sound, as seen in the Playlists Directory. This is the name all players see in the Playlists Directory sidebar while the sound is playing.</dd>
    <dt>Audio Source</dt>
    <dd>The source file for the sound. This can either be a file stored in Foundry directories, or a direct URL link to a sound file online. FVTT supports .flac, .mp3, .wav, and .webm audio files.</dd>
    <dt>Volume</dt>
    <dd>The volume of the individual sound. The sound's volume is also affected by the Master Playlists volume slider.</dd>
    <dt>Repeat</dt>
    <dd>Determines whether the sound repeats when played. Note that currently, sounds do not loop seamlessly - there will be a short gap between loops, regardless of the sound file chosen.</dd>
</dl>
<hr>

<h3 id="api">API References</h3>
<p>To interact with Playlists programmatically, consider using the following API concepts:</p>
<ul>
    <li>The @API[Playlist,The Playlist Entity] Entity</li>
	<li>The @API[Playlists,The Playlists Collection] Collection</li>
	<li>The @API[PlaylistDirectory,The PlaylistDirectory Sidebar Directory] Sidebar Directory</li>
	<li>The @API[PlaylistConfig,The PlaylistConfig Application] Application</li>
	<li>The @API[PlaylistSoundConfig,The PlaylistSoundConfig Application] Application</li>
</ul>
<hr>