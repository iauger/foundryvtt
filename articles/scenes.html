<p>Scenes represent the areas of a World that the players may explore. Scenes may depict a variety of settings ranging from world or regional maps all the way down to small buildings or dungeons. At each point in time, one Scene is classified as the <strong>active</strong> scene. The same scene is active for all users. For each individual user a different Scene may be treated as the <strong>viewed</strong> scene, which is the area currently rendered on the game canvas for that user.</p>

<h2 id="configuration">Scene Configuration</h2>
<p>The Scene configuration menu allows you to customize the structure and appearance of each area within your world. This configuration is displayed automatically when a new Scene is created, but can always be accessed by left-clicking on the Scene in the sidebar directory, or by right clicking on the Scene in the top navigation bar and selecting <strong>Configure</strong>.</p>

<figure>
	@Image[6]
	<figcaption>The Scene configuration sheet contains many important options. Each supported configuration option is described on this page.</figcaption>
</figure>

<p>The fields which you can customize while configuring a Scene are detailed below:</p>

<h3 id="appearance">Details and Dimensions</h3>
<dl>
	<dt>Scene Name</dt>
	<dd>
		<p>The title of the scene that will appear in the Scenes directory in the sidebar and in the Scene navigation bar at the top of the screen.</p>
	</dd>
    <dt>Accessibility</dt>
	<dd>
		<p>Scenes may be kept as &ldquo;GM Only&rdquo; in which case players will not be able to access them unless a Gamemaster user activates the scene or uses the 'pull to scene' function on a player. If accessibility is set to All Players, then players may choose to view the scene themselves if it is available in the navigation bar. You probably want to keep your secret dungeon level set to &ldquo;GM Only&rdquo; while alternatively it makes sense to set region map the party is exploring to be viewed by &ldquo;All Players&rdquo;.</p>
	</dd>
	<dt>Toggle Navigation</dt>
	<dd>
		<p>If this setting is enabled, the scene will appear in the Navigation bar at the top of the screen. This navigation menu is best used for scenes you plan to actively use during the course of a game session for convenient access. You can also add or remove scenes from the navigation menu by right clicking on the Scene in the sidebar directory.</p>
	</dd>
	<dt>Navigation Name</dt>
	<dd>
		<p>Override the text of the Scene Name that is displayed in the Navigation Bar for cases when it is important to avoid spoilers from revealing the true Scene name. The Scene Directory sidebar will continue to show the scene name.  </p>
	</dd>
	<dt>Background Image</dt>
	<dd>
		<p>The file path or web address of the background image you wish to use for your scene. The icon to the right of the field will open a File Picker that helps you to locate images within your public directory.</p>
	</dd>
    <dt>Scene Dimensions</dt>
	<dd>
		<p>Set the the rendered width abd height of your scene in pixels. If you do not fill this field out, the application will figure out both automatically once the image is loaded.</p>
	</dd>
    <dt>Padding Percentage</dt>
	<dd>
		<p>Creates a border region around the background image that is a percentage of the total scene dimensions. This padding space treats everything as lit and visible, but otherwise functions as normal map space. It is intended to be used for staging tokens and tiles waiting to be used in a scene - but please be aware the padding region is visible to players. When editing this after a scene is created Foundry will attempt to keep placed tokens and tiles in the padding space if its area is reduced.</p>
	</dd>
	<dt>Background Color</dt>
	<dd>
		<p>A background color to fill behind the Scene background image.</p>
	</dd>
</dl>

<h3 id="appearance">Grid Configuration</h3>
<dl>
	<dt>General Info</dt>
	<dd>
		<p>This configuration section is used to modify the grid type and alignment which is displayed as an overlay atop the Scene. Even if you are using a map that has a pre-drawn grid it is important to configure this section because the Foundry grid dictates the positions where Tokens are placed, how distance is measured, or how Measured Templates identify target spaces. Configuration of the Grid in Foundry uses three key concepts:</p>
		<ul>
			<li>The width (in pixels) of the image.</li>
			<li>The height (in pixels) of the image.</li>
			<li>The grid size (in pixels); the number of pixels which represent a single grid space.</li>
		</ul>
		<p>For example, suppose you have a map with a pre-drawn square grid which has 22 grid spaces in width and 18 grid spaces in height. The original map image is 3080px by 2520px. By knowing the dimensions of the image and by counting the number of grid spaces displayed on the map we can learn that the intended grid size for this map is 140px per square.</p>

		<p class="note info">Foundry Virtual Tabletop enforces a minimum grid size of 50px. If your map uses a grid size lower than 50px you can still use it by increasing the background size. The easiest solution is to multiply all 3 of the width, height, and grid size by two to double every dimension.</p>
		<p class="note info">If your background image has a visible grid already incorporated into the image and you are having trouble aligning the Foundry grid with the pre-existing one, be sure to use the <strong>Grid Alignment Tool</strong> by clicking the angled-ruler icon to the right of the Grid Type dropdown which has a specialized tool and workflow to assist with this process.</p>
	</dd>
	<dt>Grid Type</dt>
	<dd>
		<p>Foundry Virtual Tabletop supports multiple grid types which can be configured on a per-Scene basis. The following options are supported:</p>
		<ul>
			<li>Gridless - No fixed grid is used on this Scene allowing free-form point-to-point measurement without grid lines.</li>
			<li>Square - A square grid is used with width and height of each grid space equal to the chosen grid size.</li>
			<li>Hexagonal Columns, Odd - A column-wise hexagon grid (flat-topped) where odd-numbered rows are offset.</li>
			<li>Hexagonal Columns, Even - A column-wise hexagon grid (flat-topped) where even-numbered rows are offset.</li>
			<li>Hexagonal Rows, Odd - A row-wise hexagon grid (pointy-topped) where odd-numbered columns are offset.</li>
			<li>Hexagonal Rows, Even - A row-wise hexagon grid (pointy-topped) where even-numbered columns are offset.</li>
		</ul>
	</dd>
	<dt>Grid Size</dt>
	<dd>
		<p>The grid size (in pixels); the number of pixels which represent a single grid space.</p>
	</dd>
	<dt>Offset Background</dt>
	<dd>
		<p>Shift the background image horizontally or vertically by a number of pixels to better line up pre-drawn grid lines. Positive numbers shift the image to the right or down, negative numbers to the left or up.</p>
	</dd>
	<dt>Grid Scale</dt>
	<dd>
		<p>The unit of measure that each square of space represents in this map. For battlemaps this may be on the order of feet or meters, for a large region map each individual grid space could represent miles or kilometers. You may put whatever string value you wish in this field, it is only used for tooltip displays when measuring.</p>
	</dd>
    <dt>Grid Color</dt>
	<dd>
		<p>Customize the color of the rendered grid.</p>
	</dd>
    <dt>Grid Opacity</dt>
	<dd>
		<p>Customize the transparency of the grid. Higher opacity results in a less transparent grid, and vice versa. If you set this slider all the way to zero (left) the grid will not be visible at all.</p>
	</dd>
</dl>

<h3 id="lighting">Lighting and Vision</h3>
<dl>
    <dt>Token Vision</dt>
	<dd>
		<p>If this setting is enabled, visibility of the scene will require ownership of a Token which is placed in the scene and has a vision setting. If this setting is disabled, players will be able to see a scene even if they do not have a token present. This setting must be enabled to make use of Fog of War and Dynamic Lighting features.</p>
	</dd>
    <dt>Global Illumination</dt>
	<dd>
		<p>If this setting is enabled, the entire scene will be affected by Bright Light, allowing full visibility for any areas of the scene where an owned token has line-of-sight. This setting will have no effect if Token Vision is disabled.</p>
	</dd>
    <dt>Fog Exploration</dt>
	<dd>
		<p>If this setting is enabled, fog-of-war exploration will be tracked on a per-User basis and stored to the database as the Scene is explored. If this setting is disabled, fog exploration does not occur and instead Tokens are limited to only see their current vision polygon.</p>
	</dd>
    <dt>Darkness Level</dt>
	<dd>
		<p>Control a day/night transition for the Scene where a darkness level of zero corresponds to full daylight and a darkness level of one corresponds to the strongest level of darkness.</p>
	</dd>
	<p class="note info"> Note: setting a Darkness Level has no effect on Global Illumination.</p>
	<dt>Global Illumination Threshold</dt>
	<dd>
		<p>When toggled, this will cause Foundry to automatically disable Global Illumination if the darkness level goes over the specified threshold.</p>
	</dd>
</dl>

<h3 id="appearance">Ambience and Atmosphere</h3>
<dl>
	<dt>Journal Notes</dt>
	<dd>
		<p>An existing Journal Entry which is linked to this Scene as a set of notes which can be conveniently accessed through the context menu from the navigation or sidebar.</p>
	</dd>
	<dt>Audio Playlist</dt>
	<dd>
		<p>Define a reference to an existing Audio Playlist which will begin automatically playing when this Scene is activated.</p>
	</dd>
	<dt>Weather Effect</dt>
	<dd>
		<p>Define an available ambient weather effect which should be applied to this Scene.</p>
	</dd>
	<dt>Initial View Position</dt>
	<dd>
		<p>Define the coordinates of an initial camera position which would define the starting view position for a character which <em>does not</em> have a controlled Token in the Scene. Players who have a Token in the Scene will begin their view centered on their Token location.</p>
	</dd>
</dl>

<h2 id="context">Scene Context Options</h2>
<p>Scenes which are displayed in the Navigation bar will appear at the top of the screen. The navigation bar is intended to act similarly to "bookmarks" for commonly used Scenes which are expected to be used during the game session. You may have many more Scenes in your World than you wish to keep in the Navigation bar at any given time. Remember that players can access Scenes in the navigation bar also if they are not set to <strong>GM Only</strong>.</p>

<h4>Navigation Bar Context Menu</h4>

<figure class="right">
	@Image[37]
	<figcaption>Scenes in the Navigation Bar have<br>context menu options available on right-click.</figcaption>
</figure>

<dl>
	<dt>Activate</dt>
	<dd>Set a Scene as active, immediately loading and pulling all Users to the Scene.</dd>

	<dt>Configure</dt>
	<dd>Open the Scene Configuration sheet.</dd>

	<dt>Preload Scene</dt>
	<dd>Request that all connected Users begin loading the image and video assets needed to display this Scene in the background.</dd>

	<dt>Toggle Navigation</dt>
	<dd>Remove the Scene from the Navigation bar if it is not active.</dd>
</dl>

<h4>Scenes Sidebar Context Menu</h4>

<p>All Scenes, including those not displayed in the Navigation Bar, are available in the Scenes sidebar directory which is denoted by a tri-fold map icon. Each Scene in the sidebar has additional context menu options available.</p>

<figure class="right">
	@Image[38]
	<figcaption>Scenes in the Navigation Bar have<br>context menu options available on right-click.</figcaption>
</figure>

<dl>
	<dt>Activate</dt>
	<dd>Set a Scene as active, adding it to the Navigation Bar and immediately loading and pulling all Users to the Scene.</dd>

	<dt>Configure</dt>
	<dd>Open the Scene Configuration sheet.</dd>

	<dt>Toggle Navigation</dt>
	<dd>Add or remove the Scene from the Navigation bar if it is not active.</dd>

	<dt>Regenerate Thumbnail Image</dt>
	<dd>Rebuild a thumbnail image for the Scene background which is displayed as a preview in the Sidebar.</dd>

	<dt>Delete</dt>
	<dd>Permanently delete the Scene, you will be prompted to confirm your choice if you click this option.</dd>

	<dt>Duplicate</dt>
	<dd>Copy this Scene and all its contained data to create a new Scene which can be independently configured.</dd>

	<dt>Export Data</dt>
	<dd>Save the contents of the Scene to a JSON file which can be saved on your own computer.</dd>

	<dt>Import Data</dt>
	<dd>Load stored data for a Scene from a file on your computer, updating the Scene with the stored settings.</dd>
</dl>